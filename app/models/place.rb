class Place < ApplicationRecord
  belongs_to :user
  validates :user_id, presence: true
  default_scope -> { order(created_at: :desc) }
  geocoded_by :address, :latitude  => :lat, :longitude => :lon
  after_validation :geocode, if: ->(obj){ obj.address.present? and obj.address_changed? }
  mount_uploader :picture, PictureUploader
  
  def address
        
  end
    
  private
    
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "Debe ser menos de 5mb")
      end
    end
end
