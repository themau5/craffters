class PlacesController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy
  
  def index
    posts = []
    if filtered_places = current_user.following.each{|x|  x.microposts.each{|m| posts << m}}
      @places = Place.order('created_at DESC')
    end
  end

  def new
    @place = Place.new
  end

  def create
    @place = current_user.places.build(place_params)
    if @place.save
      flash[:success] = "Tu lugar ha sido creado!"
      redirect_to places_path
    else
      render 'new'
    end
  end
  
  def destroy
    @place.destroy
    flash[:success] = "Tu lugar ha sido borrado!"
    redirect_to places_path
  end

  private

  def place_params
    params.require(:place).permit(:title, :address, :visited_by, :picture)
  end
  
  def correct_user
   @place = current_user.places.find_by(id: params[:id])
  end
end
