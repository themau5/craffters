module UsersHelper

  # Returns the Gravatar for the given user.
  def gravatar_for(user, class: "circlee responsive-img")
    if user.avatar?
        image_tag(user.avatar.url, class: "circlee responsive-img")
    else
      render partial: 'shared/avatar'
    end
  end
  
  def cover_for(user, class: "background-size")
    if user.cover?
      image_tag(user.cover.url, class: "background-size")
    end
  end
end