class PictureUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
   # Mobile version
  version :mobile do
    process :resize_to_fit => [300, 300]
  end

  # Tablet version
  version :tablet do
    process :resize_to_fit => [600, 600]
  end

  # Desktop version
  version :desktop do
    process :resize_to_fit => [900, 900]
  end
  
  version :thumb do
    resize_to_fill(100, 100)
  end
  
  version :large do
    resize_to_limit(600, 600)
  end

  if Rails.env.production?
    storage :fog
  else
    storage :file
  end

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Add a white list of extensions which are allowed to be uploaded.
  def extension_white_list
    %w(jpg jpeg gif png)
  end
end