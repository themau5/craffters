class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user
    mail to: user.email, subject: "Activación de tu Cuenta"
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Cambio de Contraseña"
  end
end
